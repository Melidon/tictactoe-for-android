package hu.bme.aut.android.tictactoe

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.tictactoe.model.TicTacToeModel

class GameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
    }

    fun endGame(winner: Byte) {
        val text: String = when (winner) {
            TicTacToeModel.CIRCLE -> {
                getString(R.string.txt_circle_won)
            }
            TicTacToeModel.CROSS -> {
                getString(R.string.txt_cross_won)
            }
            else -> {
                getString(R.string.txt_draw)
            }
        }
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
        finish()
    }
}