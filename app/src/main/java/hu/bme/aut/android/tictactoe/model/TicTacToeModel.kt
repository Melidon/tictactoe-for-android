package hu.bme.aut.android.tictactoe.model

object TicTacToeModel {

    const val EMPTY: Byte = 0
    const val CIRCLE: Byte = 1
    const val CROSS: Byte = 2
    const val DRAW: Byte = 3

    var nextPlayer: Byte = CIRCLE

    private var model: Array<ByteArray> = arrayOf(
        byteArrayOf(EMPTY, EMPTY, EMPTY),
        byteArrayOf(EMPTY, EMPTY, EMPTY),
        byteArrayOf(EMPTY, EMPTY, EMPTY)
    )

    fun resetModel() {
        for (i in 0 until 3) {
            for (j in 0 until 3) {
                model[i][j] = EMPTY
            }
        }
    }

    fun getFieldContent(x: Int, y: Int): Byte {
        return model[x][y]
    }

    fun changeNextPlayer() {
        nextPlayer = (CIRCLE + CROSS - nextPlayer).toByte()
    }

    fun setFieldContent(x: Int, y: Int, content: Byte): Byte {
        changeNextPlayer()
        model[x][y] = content
        return content
    }

    fun whoIsWinner(): Byte {
        for (i in 0 until 3) {
            if (model[i][1] == model[i][0] && model[i][2] == model[i][0]) {
                return model[i][0]
            }
        }
        for (j in 0 until 3) {
            if (model[1][j] == model[0][j] && model[2][j] == model[0][j]) {
                return model[0][j]
            }
        }
        if (model[1][1] == model[0][0] && model[2][2] == model[0][0]) {
            return model[0][0]
        }
        if (model[1][1] == model[0][2] && model[2][0] == model[0][2]) {
            return model[0][2]
        }
        if (isFull()) {
            return DRAW;
        }
        return EMPTY

    }

    private fun isFull(): Boolean {
        for (i in 0 until 3) {
            for (j in 0 until 3) {
                if (model[i][j] == EMPTY) {
                    return false
                }
            }
        }
        return true
    }

}
